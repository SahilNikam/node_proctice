const express = require('express');
const Movie = require('../modules/movie');
const Genres = require('../modules/genre');
const Joi = require('joi');

const schema = Joi.object({
    title: Joi.string()
        .min(3)
        .max(50)
        .required(),
    numberInStock: Joi.number(),
    genreId: Joi.string(),
    dailyRentalRate: Joi.number()
});


const route = express.Router();

route.post('/',async (req, res) => {
    const obj = req.body;
    try{
        temp = schema.validate(req.body);
        if(temp.error){
            throw new Error(temp.error.details[0].message)
        }
    }
    catch(err){
        res.status(400).send(err.message);
        return;
    }
    const genre = await Genres.findById(obj.genreId);
    if(!genre){
        res.status(400).send("genra id is not valid");
    }
    const movie = new Movie({
        title: obj.title,
        genre: {
            _id: genre._id,
            name: genre.name
        },
        numberInStock: obj.numberInStock,
        dailyRentalRate: obj.dailyRentalRate
    });
    const result = await movie.save();
    if(!result){
        res.status(400).send("cannot store this value");
    }
    res.send(result);
}); 

route.get('/:id', async(req, res) => {
    const result = await Movie.findById(req.params.id);
    if(!result){
        res.send(404).send("Invalid Id");
        return;
    }
    res.send(result);
});

route.delete('/:id',async (req, res) => {
    try{
        const result = await Movie.findByIdAndDelete(req.params.id);
        if(!result){
            throw new Error("Object of given id is not present");
        }
    }
    catch(err){
        res.send(err.message);
    }
    res.send("object deleted successfully");
});

module.exports = route;

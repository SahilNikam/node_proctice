const express = require('express');
const Joi = require('joi');
const Customer = require('../modules/customer')

const route = express.Router();

const schema = Joi.object({
    name  : Joi.string().min(3).max(10),
    phone : Joi.number(),
    isGold : Joi.boolean()
});

route.post('/', async(req, res) => {
    try{
        const obj = schema.validate(req.body);
        if(obj.error){
            throw new Error("not valid schema");
        }
    }
    catch(err){
        res.status(500).send(err.message);
        return;
    }
    const customer = new Customer({
        name: req.body.name,
        phone: req.body.phone,
        isGold: req.body.isGold
    });
    const result = await customer.save();
    if(!result){
        res.status(400).send("body is not valid");
        return;
    }
    res.send(result);
});

route.get('/',async(req, res) => {
    const result = await Customer.find();
    if(!result){
        res.status(404).send("there are not enought customers records");
        return;
    }
    res.send(result);
});

route.get('/:id',async(req, res) => {
    const result = await Customer.find({ _id: req.params.id });
    if(!result){
        res.status(404).send("there are not enought customers records");
        return;
    }
    res.send(result);
});

route.put('/:id', async(req, res)=> {
    const result = await Customer.findByIdAndUpdate(req.params.id, {
        name : (req.body.name!=null) ? req.body.name : this.name,
        isGold : (req.body.isGold!=null) ? req.body.isGold : this.isGold
    });
    if(!result){
        res.status(400).send("customer record is not found");
        return;
    }
    res.send(result);
});

route.delete('/:id', async(req, res) => {
    const result = await Customer.deleteOne({_id: req.params.id});
    if(!result){
        res.status(404).send("rquested customer is not found");
        return;
    }
    res.send(result);
});

module.exports = route;
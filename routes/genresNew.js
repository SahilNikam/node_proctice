const express = require('express');
const Joi = require('joi');
const Genres = require('../modules/genre');

const route = express.Router();

const schema = Joi.object({
    name: Joi.string().min(3).max(10),
    id: Joi.number()
});

// Fawn library for transactions, for doing two way commits
// MongoDB driver
// joi-objectId
route.post('/',async (req, res) => {
    try {
        const obj = schema.validate(req.body);
        if(obj.error){
            throw new Error("not valid schema");
        }
    }
    catch(err){
        res.send(err.message);
        return;
    }
    const genra = new Genres(req.body);
    const result = await genra.save();
    res.send(result);
});

route.use(express.json());

route.get('/', async(req, res, next) => {
    try{
        const result =  await Genres.find();
        res.send(result);
    }
    catch(err){
        next(err); 
    }
});

route.get('/:id', (req, res) => {  
    async function getcource(ID){
        try{
            const result = await Genres
                            .find({ id: ID });
            if(result.length === 0){
                throw new Error("invalid course id");
            }
            res.send(result);
        }
        catch(err){
            res.send(err);
        }
    }
    getcource(parseInt(req.params.id));
});

route.put('/:id', (req, res) => {
    try{
        async function updateGenra(ID){
            // console.log("result");
            const result = await Genres
                            .find({ id: ID});
            console.log(result);
            result[0].name = req.body.name;
            const updated = await result[0].save();
            res.send(updated);
        }
        console.log("result");
        updateGenra(req.params.id);
    }
    catch(err){
        res.send(err);
    }
});

route.delete('/:id', (req, res) => {
    try{
        async function deleteGenra(ID){
            const result = await Genres.deleteOne( {id: ID});
            if(result.length === 0){
                throw new Error("genere is not available");
            }
            res.send(result);
        }
        deleteGenra(parseInt(req.params.id));
    }
    catch(err){
        res.send(err);
    }
});

module.exports = route;
const express = require('express');
const bcrypt = require('bcrypt');
const Joi = require('joi');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../modules/user');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const route = express.Router();

const schema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    isAdmin: Joi.boolean()
});

route.get('/me', auth, async(req, res) => {
    const user = await  User.findById(req.user._id).select('-password');
    res.send(user);
});

route.post('/',async(req, res) => {
    try{
        const obj = schema.validate(req.body);
        if(obj.error){
            throw new Error("not a valid schema");
        }
    }
    catch(err){
        res.status(500).send(err.message);
        return;
    }
    const user = new User(_.pick(req.body,['name', 'email', 'password','isAdmin']));
    const salt = await bcrypt.genSalt(5);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(_.pick(user, ['name', 'email']));
});

route.delete('/', [auth, admin], async(req, res) => {
    const obj = await User.findByIdAndDelete({_id: req.body.id});
    if(!obj){
        return res.status(404).send("user not found");
    }
    res.send(obj);
});

module.exports = route;

// [auth, admin],
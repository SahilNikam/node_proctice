const express = require('express');
const bcrypt = require('bcrypt');
const Joi = require('joi');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../modules/user');
const route = express.Router();

const schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required()
});

route.post('/', async(req, res) => {
    try{
        const obj = schema.validate(req.body);
        if(!obj){
            throw new Error("not a valid schema");
        }
    }
    catch(err){
        res.status(500).send(err.message);
    }
    
    const user = await User.findOne({ email: req.body.email });
    if(!user) res.status(400).send("Invalid email or password");

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) res.status(400).send("Invalid email or password");

    // res.send(true);
    const token = user.generateAuthToken();
    res.send(token);

});

module.exports = route;


// here we don't have any database connected to mongodb

const express = require('express');

const route = express.Router();
const Joi = require('joi');

let genres = [
    { id: 1, name: "Romance"},
    { id: 2, name: "Action"},
    { id: 3, name: "Biopic"}
];

function validationGenres(genra){
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(genra, schema);
};

route.use(express.json());

route.get('/', (req, res) => {
    if(!genres)
        return res.status(404).send("Movies list not found");
    res.send(genres);
});

route.get('/:id', (req, res) => {
    const genra = genres.find( g => g.id === parseInt(req.params.id));
    if(!genra) 
        return res.status(404).send("genra is not found");
    res.send(genra);
});

route.post('/', (req, res) => {
    const result = validationGenres(req.body);
    if(result.error)
        return res.status(400).send("Format is not proper");
    const genra = {
        id : genres.length + 1,
        name : req.body.name
    };
    genres.push(genra);
    res.send(genra);
});

route.put('/:id', (req, res) => {
    const genra = genres.find( g => g.id === parseInt(req.params.id));
    if(!genra)
        return res.status(404).send("genra is not found");
    const result = validationGenres(req.body);
    if(result.error)
        return res.status(400).send("Format is not proper");
    genra.name = req.body.name;
    res.send(genra);
});

route.delete('/:id', (req, res) => {
    const genra = genres.find( g => g.id === parseInt(req.params.id));
    if(!genra) 
        return res.status(404).send("genra is not found");
    genres = genres.filter(g => g !== genra);
    res.send(genra);
});

module.exports = route;
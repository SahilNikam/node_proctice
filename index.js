require('express-async-errors');
require('winston-mongodb');
const winston = require('winston');
const express = require('express');
const mongoose = require('mongoose');
const config = require('config');

const movie = require('./routes/movies')
const genres = require('./routes/genresNew');
const customers = require('./routes/customers');
const users = require('./routes/users');
const auth = require('./routes/auth');
const error = require("./middleware/error");

process.on('uncaughtException', (ex)=> { // this is for handling exceptions which are outside of the scope of the express
    console.log("Got Uncaught Exception");
    winston.error(ex.message, ex);
});

process.on('unhandledRejection', (ex)=> { // this is for handling unhandledRejection of promise
    console.log("Got Uncaught Rejection");
    winston.error(ex.message, ex);
});

if(!config.get('jwtPrivateKey')) {
    console.error('FATAL ERROR: jwtPrivateKey is not defined');
    process.exit(1);
}

mongoose.connect('mongodb://localhost:27017/genres')
    .then(console.log("Connected to the customers database"))
    .catch(error =>  console.log('cannot connect to the customers database', error));

const app =  express();
app.use(express.json());
app.use('/genres', genres);
app.use('/customers', customers);
app.use('/movies', movie);
app.use('/users', users);
app.use('/auth', auth);

winston.add(winston.transports.File, { filename: 'logfile.log'}); // stetting log file for errors.
// winston.add(winston.transports.MongoDB, { db: 'mongodb://localhost:27017/genres' });


app.use(error);

app.listen(3000,() => console.log("Listening the port 3000"));

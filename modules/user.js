const mongoose = require('mongoose');
const config = require('config');
const jwt = require('jsonwebtoken');
const boolean = require('joi/lib/types/boolean');

const schema = new mongoose.Schema({
    name : {
        type: String,
        min: 5,
        max: 12
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String,
        min: 8,
        max: 12
    },
    isAdmin :{
        type: Boolean
    }
});

schema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id: this._id, isAdmin: this.isAdmin }, config.get('jwtPrivateKey'));
    return token;
}

const User = mongoose.model('Users', schema);



module.exports = User;
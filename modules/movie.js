const mongoose = require('mongoose');
const Genre = require('./genre');

// const genresSchems = new mongoose.Schema({
//     name: {
//         type: String,
//         required: true,
//         lowercase: true
//     }
// });

// const Genre = mongoose.model('Genre', genresSchems);

const moviesSchems = new mongoose.Schema({
    title: {
        type: String,
        // required: true
    },
    genre: {
        _id:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Genre'
        },
        name : String
    },
    numberInStock: Number,
    dailyRentalRate: Number
});

const Movies = mongoose.model('Movies', moviesSchems);

module.exports = Movies;
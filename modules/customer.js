const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
    isGold : {
        type: Boolean,
        default: false
    },
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    }, 
    phone: {
        type: Number,
        required: true
    }
});

const Customer = mongoose.model('customers', customerSchema);

module.exports = Customer;
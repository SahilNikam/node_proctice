const mongoose = require('mongoose');

const genresSchems = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true
    },
    id: {
        type: Number
    }
});

const Genres = mongoose.model('Genres', genresSchems);

module.exports = Genres;